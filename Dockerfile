FROM hub.eole.education/proxyhub/bitnami/git:latest as BUILD
COPY app-version /tmp/
RUN cd /tmp && \
    git clone https://gitlab.mim-libre.fr/EOLE/eole-3/services/ladigitale/digiboard/digiboard-sources.git && \
    cd /tmp/digiboard-sources && \
    git checkout $(cat ../app-version) && \
    cp -a /tmp/digiboard-sources /tmp/src
FROM hub.eole.education/proxyhub/library/node:16-alpine AS NODE
COPY --from=BUILD /tmp/src /src
WORKDIR src
RUN npm install -g npm@8.19.4
RUN npm install -g vite
RUN npm install
RUN npm run build
FROM hub.eole.education/proxyhub/alpine:latest
COPY --from=NODE /usr/lib /usr/lib
COPY --from=NODE /usr/local/lib /usr/local/lib
COPY --from=NODE /usr/local/include /usr/local/include
COPY --from=NODE /usr/local/bin /usr/local/bin
COPY --from=NODE /src /src
WORKDIR /src
ENTRYPOINT ["npm", "run", "prod"]
EXPOSE 3000
